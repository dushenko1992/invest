"use strict";

$(document).ready(function () {
  $(function () {
    $(".accordion__item h3").on("click", function (e) {
      e.preventDefault();
      var $this = $(this);

      if (!$this.hasClass("active")) {
        $(".accordion__content").slideUp(400);
        $(".accordion__item h3").removeClass("active");
      }

      $this.toggleClass("active");
      $this.next().slideToggle();
    });
  });
});
$(document).ready(function () {
  //burger
  $(".js-burger").click(function () {
    $(this).toggleClass("active");
    $(".js-nav").toggleClass("active");
    $("body").toggleClass("overflow");
  }); //reg form

  $(".js-member").click(function () {
    $(".js-user-form").toggleClass('active');
  });
}); // $(document).click( function(e){
//   if ( $(e.target).closest('.js-user-form').length ) {
//       return;
//   }
//   $('.js-user-form').removeClass('active');
// });

$(document).ready(function () {
  //tabs
  $(function () {
    $("ul.js-tab-btn").on("click", "li:not(.active)", function () {
      $(this).addClass("active").siblings().removeClass("active").closest("div.js-tabs").find("div.js-tab-content").removeClass("active").eq($(this).index()).addClass("active");
    });
  }); //tabs-in-tabs

  $(function () {
    $("ul.js-btn").on("click", "li:not(.show)", function () {
      $(this).addClass("show").siblings().removeClass("show").closest("div.js-tab").find("div.js-content").removeClass("show").eq($(this).index()).addClass("show");
    });
  });
});
$(document).ready(function () {
  $(".loader-wrap").addClass("hide"); //validation form
  // $("#formClass").validate({
  //   ignore: [],
  //   rules: {
  //     user_name: { required: true },
  //     user_phone: { required: true },
  //     select_class: { required: true },
  //   },
  //   messages: {
  //     user_name: "Please enter your name",
  //     user_phone: "Please enter your phone number",
  //     select_class: "Please select a class",
  //   },
  // });
  //question

  $(".js-one-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-one-question").removeClass("active");
    $(".js-two-question").addClass("active");
  });
  $(".js-two-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-two-question").removeClass("active");
    $(".js-three-question").addClass("active");
  });
  $(".js-three-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-three-question").removeClass("active");
    $(".js-four-question").addClass("active");
  });
  $(".js-four-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-four-question").removeClass("active");
    $(".js-five-question").addClass("active");
  }); //page nav

  $(".js-open-faq").click(function () {
    // $(".js-tab-btn li").removeClass("active");
    // $(".js-tab-content").removeClass("active");
    $(".js-faq-tab").addClass("active");
  }); //plan

  $(".js-month").click(function () {
    $(".js-rate").removeClass("active");
  });
  $(".js-year").click(function () {
    $(".js-rate").addClass("active");
  }); //animation

  AOS.init();
});