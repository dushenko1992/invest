$(document).ready(function () {
  //tabs
  $(function () {
    $("ul.js-tab-btn").on("click", "li:not(.active)", function () {
      $(this)
        .addClass("active")
        .siblings()
        .removeClass("active")
        .closest("div.js-tabs")
        .find("div.js-tab-content")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
    });
  });
  
  //tabs-in-tabs
  $(function () {
    $("ul.js-btn").on("click", "li:not(.show)", function () {
      $(this)
        .addClass("show")
        .siblings()
        .removeClass("show")
        .closest("div.js-tab")
        .find("div.js-content")
        .removeClass("show")
        .eq($(this).index())
        .addClass("show");
    });
  });
});
