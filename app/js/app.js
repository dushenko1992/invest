//=require ../blocks/**/*.js
$(document).ready(function () {
  $(".loader-wrap").addClass("hide");

  //validation form
  // $("#formClass").validate({
  //   ignore: [],
  //   rules: {
  //     user_name: { required: true },
  //     user_phone: { required: true },
  //     select_class: { required: true },
  //   },
  //   messages: {
  //     user_name: "Please enter your name",
  //     user_phone: "Please enter your phone number",
  //     select_class: "Please select a class",
  //   },
  // });

  //question
  $(".js-one-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-one-question").removeClass("active");
    $(".js-two-question").addClass("active");
  });
  $(".js-two-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-two-question").removeClass("active");
    $(".js-three-question").addClass("active");
  });
  $(".js-three-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-three-question").removeClass("active");
    $(".js-four-question").addClass("active");
  });
  $(".js-four-btn").click(function () {
    $(this).toggleClass("active");
    $(".js-four-question").removeClass("active");
    $(".js-five-question").addClass("active");
  });

  //page nav
  $(".js-open-faq").click(function () {
    // $(".js-tab-btn li").removeClass("active");
    // $(".js-tab-content").removeClass("active");
    $(".js-faq-tab").addClass("active");
  });
  
  //plan
  $(".js-month").click(function () {
    $(".js-rate").removeClass("active");
  });
  $(".js-year").click(function () {
    $(".js-rate").addClass("active");
  });
  

  //animation
  AOS.init();
});
